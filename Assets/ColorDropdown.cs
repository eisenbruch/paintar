﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorDropdown : MonoBehaviour {

    public GameObject paintObject;
    TrailRenderer paint;

    Dropdown m_Dropdown;

	public void Start()
	{
		paint = paintObject.GetComponentInChildren<TrailRenderer>();

        //Fetch the Dropdown GameObject
        m_Dropdown = GetComponent<Dropdown>();
        //Add listener for when the value of the Dropdown changes, to take action
        m_Dropdown.onValueChanged.AddListener(delegate {
            SelectColor_dropdown(m_Dropdown);
        });
	}

    void SelectColor_dropdown(Dropdown selection) 
    {
        paint.material = Resources.Load("Colors/" + selection.options[selection.value].text, typeof(Material)) as Material;
    }

}
