﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IntroPaneControl : MonoBehaviour {

    public GameObject introPanel1;
    public GameObject introPanel2;
    
	public GameObject paintPanel;

    public GameObject videoPanel;
    public GameObject adMobControl;

	public void Start()
	{
        introPanel1.SetActive(true);
        introPanel2.SetActive(false);
		paintPanel.SetActive(false);
        videoPanel.SetActive(false);
	}

	public void DisableIntroPane1() {
        introPanel1.SetActive(false);
        introPanel2.SetActive(true);
    }

    public void DisableIntroPane2()
    {
        introPanel1.SetActive(false);
        introPanel2.SetActive(false);
		paintPanel.SetActive(true);
        adMobControl.GetComponent<AdMob>().RequestBanner();
    }
}
