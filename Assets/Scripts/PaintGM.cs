﻿//-----------------------------------------------------------------------
// <copyright file="HelloARController.cs" company="Google">
//
// Copyright 2017 Google Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// </copyright>
//-----------------------------------------------------------------------

namespace GoogleARCore.HelloAR
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Rendering;
    using GoogleARCore;
    using UnityEngine.UI;
    using UnityEngine.EventSystems;


    public class PaintGM : MonoBehaviour
    {
        public Camera m_firstPersonCamera;
        public GameObject drawObj;
        //public GameObject palette;

        public GameObject TrackedPlanePrefab;

        /// <summary>
        /// A list to hold new planes ARCore began tracking in the current frame. This object is used across
        /// the application to avoid per-frame allocations.
        /// </summary>
		List<DetectedPlane> m_NewPlanes = new List<DetectedPlane>(); 

        /// <summary>
        /// A list to hold all planes ARCore is tracking in the current frame. This object is used across
        /// the application to avoid per-frame allocations.
        /// </summary>
        List<DetectedPlane> m_AllPlanes = new List<DetectedPlane>();

        GameObject obj;

        public GameObject[] drawnObjects;

        bool userHasDrawn = false;

        //public Slider scaleSlider;
        public GameObject scaleDropdown;
        //float trailWidth;

        public Toggle drawOnPlanesToggle;

        /// True if the app is in the process of quitting due to an ARCore connection error, otherwise false.
        bool m_IsQuitting = false;

        bool createdAnchor = false;

        Anchor mainAnchor;

		public GameObject searchingPanel;
		public GameObject paintPanel;
		public GameObject videoPanel;

		public void Update()
        {

			_UpdateApplicationLifecycle();

            drawnObjects = GameObject.FindGameObjectsWithTag("DrawnObject");
            
            // Check that motion tracking is tracking.
            //if (Session.Status != SessionStatus.Tracking)
            //{
            //    const int lostTrackingSleepTimeout = 15;
            //    Screen.sleepTimeout = lostTrackingSleepTimeout;
            //    if (!m_IsQuitting && Session.Status.IsValid())
            //    {
            //        SearchingForPlaneUI.SetActive(true);
            //    }

            //    return;
            //}

            Screen.sleepTimeout = SleepTimeout.NeverSleep;


            //if (!userHasDrawn)
            //{
            //    Vector3 placePlanePoint = Vector3.zero;
            //    placePlanePoint.y = 0.001f;
            //    // Iterate over planes found in this frame and instantiate corresponding GameObjects to visualize them.
            //    Session.GetTrackables<DetectedPlane>(m_NewPlanes, TrackableQueryFilter.New);
            //    for (int i = 0; i < m_NewPlanes.Count; i++)
            //    {

            //        // Instantiate a plane visualization prefab and set it to track the new plane. The transform is set to
            //        // the origin with an identity rotation since the mesh for our prefab is updated in Unity World
            //        // coordinates.
            //        GameObject planeObject = Instantiate(TrackedPlanePrefab, placePlanePoint, Quaternion.identity,
            //            transform);
            //        planeObject.GetComponent<TrackedPlaneVisualizer>().Initialize(m_NewPlanes[i]);
            //    }
            //}
            

			if(videoPanel.activeInHierarchy == false)
			{
				if (Session.Status == SessionStatus.Tracking)
				{
					paintPanel.SetActive(true);
					searchingPanel.SetActive(false);
				}
				else
				{
					paintPanel.SetActive(false);
					searchingPanel.SetActive(true);
				}            	
			}


            //CREATE ANCHOR FROM PHONE POSITION
            //if (!createdAnchor && Session.Status == SessionStatus.Tracking){
            //    mainAnchor = Session.CreateAnchor(Pose.identity);
            //    createdAnchor = true;
            //}


            if (Input.touchCount < 1)
            {
                return;
            }

            if (Input.touchCount == 1)
            {
                Touch touch; 
                touch = Input.GetTouch(0);

                // Ignore touches on UI and palette
                if (EventSystem.current.IsPointerOverGameObject(touch.fingerId)) return; 
                //Collider paletteCollider = palette.GetComponentInChildren<MeshCollider>();
                //RaycastHit h;
                //Physics.Raycast(Camera.main.ScreenPointToRay(touch.position), out h);
                //if (h.collider == paletteCollider) return;

                       //if ( == palette.GetComponentInChildren<MeshCollider>()) return;

                Vector3 screenTouch = touch.position;
                screenTouch.z = 0.333f; //DISTANCE FROM CAMERA
                Vector3 cameraPoint = m_firstPersonCamera.ScreenToWorldPoint(screenTouch); // + m_firstPersonCamera.transform.forward * distance;

                bool onPlane = false;

                TrackableHit hit;
                TrackableHitFlags raycastFilterPointCloud = TrackableHitFlags.FeaturePoint;
                TrackableHitFlags raycastFilterPlane = TrackableHitFlags.PlaneWithinInfinity;


                if (touch.phase == TouchPhase.Began)
                {
                    //Firebase.Analytics.FirebaseAnalytics.LogEvent("line_started", "param", "value");

                    Frame.Raycast(touch.position.x, touch.position.y, raycastFilterPointCloud, out hit);
                   
                    obj = Instantiate(drawObj, cameraPoint, hit.Pose.rotation);
                    //var anchor = hit.Trackable.CreateAnchor(hit.Pose);
                    if (!createdAnchor)
                    {
                        mainAnchor = hit.Trackable.CreateAnchor(hit.Pose);
                        createdAnchor = true;
                    }

                    obj.transform.parent = mainAnchor.transform;

                    userHasDrawn = true;
                    // Disable the rendering of the tracked planes
                    //TrackedPlanePrefab.GetComponent<TrackedPlaneVisualizer>().OnTogglePlanes(false);

                    ///// MAKE THEM ALL BE PARENTED TO A SINGLE ANCHOR --- ONCE TRACKING CREATE ANCHOR AT CAMERA POSE??
                    /// ///  REDUCE INITIAL WIDTH AND WIDTH RANGE
                    /// 


                    /*
                     * if (drawOnPlanesToggle.isOn && Frame.Raycast(touch.position.x, touch.position.y, raycastFilterPlane, out hit))
                    {
                        obj = Instantiate(drawObj, hit.Pose.position, hit.Pose.rotation);
                        var anchor = hit.Trackable.CreateAnchor(hit.Pose);
                        obj.transform.parent = anchor.transform;

                        onPlane = true;

                        userHasDrawn = true;
                        // Disable the rendering of the tracked planes
                        TrackedPlanePrefab.GetComponent<TrackedPlaneVisualizer>().OnTogglePlanes(false);

                    }
                    else
                    {
                        Frame.Raycast(touch.position.x, touch.position.y, raycastFilterPointCloud, out hit);

                        obj = Instantiate(drawObj, cameraPoint, hit.Pose.rotation);
                        var anchor = hit.Trackable.CreateAnchor(hit.Pose);
                        obj.transform.parent = anchor.transform;

                        //onPlane = false;


                        userHasDrawn = true;
                        // Disable the rendering of the tracked planes
                        TrackedPlanePrefab.GetComponent<TrackedPlaneVisualizer>().OnTogglePlanes(false);

                    }
                    */


                }

                if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
                {
                    if (onPlane)
                    {
                        Frame.Raycast(touch.position.x, touch.position.y, raycastFilterPlane, out hit);
                        Vector3 newPos = Vector3.MoveTowards(obj.transform.position, hit.Pose.position, 1);
                        obj.transform.position = newPos;
                    }
                    else
                    {
                        Vector3 newPos = Vector3.MoveTowards(obj.transform.position, cameraPoint, 1);
                        obj.transform.position = newPos;


                    }

                    // Disable the rendering of the tracked planes
                    //TrackedPlanePrefab.GetComponent<TrackedPlaneVisualizer>().OnTogglePlanes(false);

                    //Set trail width based on slider value
					obj.GetComponentInChildren<TrailRenderer>().endWidth = scaleDropdown.GetComponent<SizeDropdown>().width;
					obj.GetComponentInChildren<TrailRenderer>().startWidth = scaleDropdown.GetComponent<SizeDropdown>().width;

                }
            }
        }

        // Delete all items
        public void DeleteAll()
        {
            foreach (GameObject drawnObject in drawnObjects)
            {
                Destroy(drawnObject);
            }
            userHasDrawn = false;
            // Enable the rendering of the tracked planes
            //TrackedPlanePrefab.GetComponent<TrackedPlaneVisualizer>().OnTogglePlanes(true);

            //Firebase.Analytics.FirebaseAnalytics.LogEvent("deleted_line", "all_lines", "all_lines");
        }

        // Delete last placed item
        public void DeleteLast()
        {
            //int objectToDelete = drawnObjects.GetUpperBound(0);
            //Destroy(drawnObjects[objectToDelete]);
            Destroy(drawnObjects[drawnObjects.GetUpperBound(0)]);

            //Firebase.Analytics.FirebaseAnalytics.LogEvent("deleted_line", "last_line", "last_line");
        }



		/// <summary>
        /// Check and update the application lifecycle.
        /// </summary>
        private void _UpdateApplicationLifecycle()
        {
            // Exit the app when the 'back' button is pressed.
            if (Input.GetKey(KeyCode.Escape))
            {
                Application.Quit();
            }

            // Only allow the screen to sleep when not tracking.
            if (Session.Status != SessionStatus.Tracking)
            {
                const int lostTrackingSleepTimeout = 15;
                Screen.sleepTimeout = lostTrackingSleepTimeout;
            }
            else
            {
                Screen.sleepTimeout = SleepTimeout.NeverSleep;
            }

            if (m_IsQuitting)
            {
                return;
            }

            // Quit if ARCore was unable to connect and give Unity some time for the toast to appear.
            if (Session.Status == SessionStatus.ErrorPermissionNotGranted)
            {
                _ShowAndroidToastMessage("Camera permission is needed to run this application.");
                m_IsQuitting = true;
                Invoke("_DoQuit", 0.5f);
            }
            else if (Session.Status.IsError())
            {
                _ShowAndroidToastMessage("ARCore encountered a problem connecting.  Please start the app again.");
                m_IsQuitting = true;
                Invoke("_DoQuit", 0.5f);
            }
        }

        /// <summary>
        /// Actually quit the application.
        /// </summary>
        private void _DoQuit()
        {
            Application.Quit();
        }

        /// <summary>
        /// Show an Android toast message.
        /// </summary>
        /// <param name="message">Message string to show in the toast.</param>
        private void _ShowAndroidToastMessage(string message)
        {
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

            if (unityActivity != null)
            {
                AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
                unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
                {
                    AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity,
                        message, 0);
                    toastObject.Call("show");
                }));
            }
        }
    }
}
