﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Palette : MonoBehaviour {

    Vector3 palettePosition;
    Quaternion paletteRotation;
    Transform cameraTransform;

    public GameObject paintObject;
    TrailRenderer paint;

	// Use this for initialization
	void Start () {
        cameraTransform = GameObject.FindWithTag("MainCamera").transform;
        paint = paintObject.GetComponent<TrailRenderer>();
	}
	
	// Update is called once per frame
	void LateUpdate () {
        // Move palette with but below camera
        palettePosition = cameraTransform.position;
        palettePosition.y = cameraTransform.position.y - 1.0f;
        //transform.position = Vector3.MoveTowards(transform.position, palettePosition, 1);
        transform.position = Vector3.Lerp(transform.position, palettePosition, 1);

        // Match palette rotation to camera rotation
        paletteRotation = cameraTransform.rotation;
        paletteRotation.x = 0.0f;
        paletteRotation.z = 0.0f;
        transform.rotation = Quaternion.Lerp(transform.rotation, paletteRotation, 1);

    }


    // Color selection
    public void SelectColor_Red1 ()
    {
        paint.material = Resources.Load("Colors/BrushMaterial-red1", typeof(Material)) as Material;
        //LogFirebaseColorChangeEvent(paint.material);
    }

    public void SelectColor_Red2()
    {
        paint.material = Resources.Load("Colors/BrushMaterial-red2", typeof(Material)) as Material;
        //LogFirebaseColorChangeEvent(paint.material);
    }

    public void SelectColor_Yellow()
    {
        paint.material = Resources.Load("Colors/BrushMaterial-yellow", typeof(Material)) as Material;
        //LogFirebaseColorChangeEvent(paint.material);
    }

    public void SelectColor_Green()
    {
        paint.material = Resources.Load("Colors/BrushMaterial-green", typeof(Material)) as Material;
        //LogFirebaseColorChangeEvent(paint.material);
    }

    public void SelectColor_Blue1()
    {
        paint.material = Resources.Load("Colors/BrushMaterial-blue1", typeof(Material)) as Material;
        //LogFirebaseColorChangeEvent(paint.material);
    }

    public void SelectColor_Blue2()
    {
        paint.material = Resources.Load("Colors/BrushMaterial-blue2", typeof(Material)) as Material;
        //LogFirebaseColorChangeEvent(paint.material);
    }

    public void SelectColor_Pink()
    {
        paint.material = Resources.Load("Colors/BrushMaterial-pink", typeof(Material)) as Material;
        //LogFirebaseColorChangeEvent(paint.material);
    }

    public void SelectColor_White()
    {
        paint.material = Resources.Load("Colors/BrushMaterial-white", typeof(Material)) as Material;
        //LogFirebaseColorChangeEvent(paint.material);
    }

    public void SelectColor_Black()
    {
        paint.material = Resources.Load("Colors/BrushMaterial-black", typeof(Material)) as Material;
        //LogFirebaseColorChangeEvent(paint.material);
    }



    //private void LogFirebaseColorChangeEvent(Material m)
    //{
    //    Firebase.Analytics.FirebaseAnalytics.LogEvent("color_changed", "color", "color");
    //}
}
