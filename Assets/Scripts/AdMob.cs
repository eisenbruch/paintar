﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;

public class AdMob : MonoBehaviour {

    private BannerView bannerView;
    public bool adMobTestingModeEnabled;

    string prodAppId = "ca-app-pub-9005751829250824~4461914768";
    string prodAdUnitId = "ca-app-pub-9005751829250824/8864194201";

    string testAppId = "ca-app-pub-3940256099942544~3347511713";
    string testAdUnitId = "ca-app-pub-3940256099942544/6300978111";

    string appId;
    string adUnitId;

	// Use this for initialization
	void Start () {
        #if UNITY_ANDROID
        if (!adMobTestingModeEnabled)
        {
            appId = prodAppId;
        } else {
            appId = testAppId;
        }

        #else
            string appId = "unexpected_platform";
        #endif

        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);

		RequestBanner();
	}

    public void RequestBanner()
    {
        #if UNITY_ANDROID
        if(!adMobTestingModeEnabled){
            adUnitId = prodAdUnitId;
        } else {
            adUnitId = testAdUnitId;
        }
        #else
            string adUnitId = "unexpected_platform";
        #endif

        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Top);

        // Called when an ad request has successfully loaded.
        bannerView.OnAdLoaded += HandleOnAdLoaded;
        // Called when an ad request failed to load.
        bannerView.OnAdFailedToLoad += HandleOnAdFailedToLoad;
        // Called when an ad is clicked.
        bannerView.OnAdOpening += HandleOnAdOpened;
        // Called when the user returned from the app after an ad click.
        bannerView.OnAdClosed += HandleOnAdClosed;
        // Called when the ad click caused the user to leave the application.
        bannerView.OnAdLeavingApplication += HandleOnAdLeftApplication;

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().AddTestDevice("408F078EC37B71B8").Build(); // 408F078EC37B71B8
        
        // Load the banner with the request.
        bannerView.LoadAd(request);
    }

    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLoaded event received");
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print("HandleFailedToReceiveAd event received with message: "
                            + args.Message);
    }

    public void HandleOnAdOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdOpened event received");
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdClosed event received");
    }

    public void HandleOnAdLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLeftApplication event received");
    }
}