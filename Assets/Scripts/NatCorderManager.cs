﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NatCorderU.Core;
//using NatCorderU.Extensions;
//using NatShareU;
using UnityEngine.UI;
using UnityEngine.Video;
using GoogleARCore;
using NatCorderU.Core.Recorders;
using NatCorderU.Core.Clocks;

public class NatCorderManager : MonoBehaviour
{
    //public AudioSource audioSource;
    string videoPath;
    public GameObject videoPanel;
	public GameObject paintPanel;
	public GameObject searchingPanel;
	public GameObject aRCoreDevice;
    VideoPlayer videoPlayer;

	public GameObject AdMob;

	public Container container = Container.MP4;
	private CameraRecorder videoRecorder;
	public bool recordMicrophone;
    public AudioSource microphoneSource;
	private AudioRecorder audioRecorder;
	private IClock recordingClock;


	public void Start()
	{
        //PERMISSIONS
        if (!UniAndroidPermission.IsPermitted(AndroidPermission.WRITE_EXTERNAL_STORAGE)) 
        {
            RequestPermission();
        }
        
	}

	public void StartRecording()
    {
		// First make sure recording microphone is only on MP4
        recordMicrophone &= container == Container.MP4;
        // Create recording configurations // Clamp video width to 720
        var width = 720;
        var height = width * Screen.height / Screen.width;
        var framerate = container == Container.GIF ? 10 : 30;
        var videoFormat = new VideoFormat(width, (int)height, framerate);
        var audioFormat = recordMicrophone ? AudioFormat.Unity : AudioFormat.None;

		// Start recording
        NatCorder.StartRecording(container, videoFormat, audioFormat, OnReplay);
        videoRecorder = CameraRecorder.Create(Camera.main, recordingClock);

		if (recordMicrophone)
        {
            StartMicrophone();
            audioRecorder = AudioRecorder.Create(microphoneSource, true, recordingClock);
        }


        // Start recording with microphone audio
        //Replay.StartRecording(Camera.main, configuration, OnReplay);//, audioSource, false);
        //Firebase.Analytics.FirebaseAnalytics.LogEvent("started_recording", "recording", "recording");
    }

	private void StartMicrophone()
    {
#if !UNITY_WEBGL || UNITY_EDITOR // No `Microphone` API on WebGL :(
        // Create a microphone clip
        microphoneSource.clip = Microphone.Start(null, true, 60, 48000);
        while (Microphone.GetPosition(null) <= 0) ;
        // Play through audio source
        microphoneSource.timeSamples = Microphone.GetPosition(null);
        microphoneSource.loop = true;
        microphoneSource.Play();
#endif
    }
    
	public void StopRecording()
    {
        // Stop the microphone if we used it for recording
        if (recordMicrophone)
        {
            Microphone.End(null);
            microphoneSource.Stop();
            audioRecorder.Dispose();
        }
        // Stop the recording
        videoRecorder.Dispose();
        NatCorder.StopRecording();
    }

    public void ToggleRecording()
    {
		if (!NatCorder.IsRecording){
            StartRecording();
        } else {
            StopRecording();
        } 
    }

	// Called by NatCorder when recording has been completed
	void OnReplay(string path)
    {
        // Save the path to the replay so we can use it in `OnShare` and `SaveVideo`
        videoPath = path;

        // Display the Video panel
        videoPanel.SetActive(true);
        paintPanel.SetActive(false);
		searchingPanel.SetActive(false);

        // Grab Video player, set url, and play video
        videoPlayer = videoPanel.GetComponentInChildren<VideoPlayer>();
        videoPlayer.url = path;

        //videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
        //videoPlayer.EnableAudioTrack(0, true);
        //videoPlayer.SetTargetAudioSource(0, audioSource);

        videoPlayer.Prepare();

		videoPlayer.Play();

		//Handheld.PlayFullScreenMovie(path);
        // Pause ARCore session
        aRCoreDevice.GetComponent<ARCoreSession>().enabled = false;

		AdMob.GetComponent<AdMobInterstitial>().RequestInterstitial();
    }

    // Invoked by UI when user clicks share button
    public void OnShare()
    {
        // Share the replay using the native sharing UI
        NatShareU.NatShare.Share(videoPath);
        //Firebase.Analytics.FirebaseAnalytics.LogEvent("shared_recording", "recording", "recording");
        //Firebase.Analytics.FirebaseAnalytics.LogEvent(Firebase.Analytics.FirebaseAnalytics.EventShare);
    }

    public void SaveVideo()
    {
        NatShareU.NatShare.SaveToCameraRoll(videoPath);
        _ShowAndroidToastMessage("Video saved!");
        //Firebase.Analytics.FirebaseAnalytics.LogEvent("saved_recording", "recording", "recording");
    }

    public void CloseVideoPanel()
    {
        videoPanel.SetActive(false);
		AdMob.GetComponent<AdMobInterstitial>().ShowInterstitial();
        paintPanel.SetActive(true);

        // Resume ARCore session
        aRCoreDevice.GetComponent<ARCoreSession>().enabled = true;
        //Firebase.Analytics.FirebaseAnalytics.LogEvent("closed_panel", "recording", "recording");
    }

    // Show an Android toast message.
    private void _ShowAndroidToastMessage(string message)
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        if (unityActivity != null)
        {
            AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
            unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            {
                AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity,
                    message, 0);
                toastObject.Call("show");
            }));
        }
    }

    public void RequestPermission()
    {
        UniAndroidPermission.RequestPermission(AndroidPermission.WRITE_EXTERNAL_STORAGE, OnAllow, OnDeny, OnDenyAndNeverAskAgain);
    }

    private void OnAllow()
    {
        _ShowAndroidToastMessage("Permission granted!");// execute action that uses permitted function.
    }

    private void OnDeny()
    {
        _ShowAndroidToastMessage("You must enable Storage permissions to record video.");// back screen / show warnking window
        return;
    }

    private void OnDenyAndNeverAskAgain()
    {
        _ShowAndroidToastMessage("You must enable Storage permissions to record video.");// show warning window and open app permission setting page
        return;
    }

}
