﻿using GoogleARCore;
using System.Collections.Generic;
using UnityEngine;

public class ARSurfaceManager : MonoBehaviour
{
    [SerializeField] public Material m_surfaceMaterial;
	List<DetectedPlane> m_newPlanes = new List<DetectedPlane>();


    void Update()
    {
#if UNITY_EDITOR
        return;
#endif

        if (Session.Status != SessionStatus.Tracking)
        {
            return;
        }

		Session.GetTrackables<DetectedPlane>(m_newPlanes, TrackableQueryFilter.New);

        foreach (var plane in m_newPlanes)
        {
            var surfaceObj = new GameObject("ARSurface");
            surfaceObj.tag = "ARSurface";
            surfaceObj.AddComponent<ARSurface>().SetTrackedPlane(plane, m_surfaceMaterial);
        }
    }
}
